# 陈登职校手持式口腔照相app

本app是为佛山市顺德区陈登职业技术学校创客实验室定制修改的一个外包安卓app，支持使用该校创客实验室开发的手持式口腔摄像仪在手机端进行口腔拍照，可供该校师生学习参考

## 功能
1. 外置USB摄像头拍照
2. 连拍
3. 对焦
4. 设置分辨率
5. 调整亮度对比度
6. 设置连拍间隔
7. 相册分享功能
8. 设置连拍间隔
9. 摄像头硬件按钮点击直接连拍三张

## apk下载链接
[https://gitee.com/fsfzp888/OralCameraApp/raw/master/release/release/app-release.apk](https://gitee.com/fsfzp888/OralCameraApp/raw/master/release/release/app-release.apk)


## 支持的安卓版本
Android 5.0+

## 启动界面
![](./release/startup_view.png)

## 操作界面
![](./release/capture_view.jpg)

## 参考
1. [saki4510t/UVCCamera](https://github.com/saki4510t/UVCCamera)
2. [jiangdongguo/AndroidUSBCamera](https://github.com/jiangdongguo/AndroidUSBCamera)
