package com.serenegiant.usb.encoder;

import android.content.Context;

public class PathParams {
    private String mSavePath;
    private String mWaterMark;
    private Context mContext;

    public String getmSavePath() {
        return mSavePath;
    }

    public void setmSavePath(String mSavePath) {
        this.mSavePath = mSavePath;
    }

    public String getmWaterMark() {
        return mWaterMark;
    }

    public void setmWaterMark(String mWaterMark) {
        this.mWaterMark = mWaterMark;
    }

    public Context getmContext() {
        return mContext;
    }

    public void setmContext(Context mContext) {
        this.mContext = mContext;
    }
}
