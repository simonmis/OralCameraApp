package com.zpfeng.oralcamera.utils;

import androidx.annotation.StringDef;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;


@StringDef({ShareContentType.TEXT, ShareContentType.IMAGE,
        ShareContentType.AUDIO, ShareContentType.VIDEO, ShareContentType.FILE})
@Retention(RetentionPolicy.SOURCE)
public @interface ShareContentType {
    /**
     * Share Text
     */
    String TEXT = "text/plain";

    /**
     * Share Image
     */
    String IMAGE = "image/*";

    /**
     * Share Audio
     */
    String AUDIO = "audio/*";

    /**
     * Share Video
     */
    String VIDEO = "video/*";

    /**
     * Share File
     */
    String FILE = "*/*";
}
