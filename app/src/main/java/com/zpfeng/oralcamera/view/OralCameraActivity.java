package com.zpfeng.oralcamera.view;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.hardware.usb.UsbDevice;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Looper;
import android.text.InputType;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.Surface;
import android.view.TextureView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ListView;
import android.widget.SeekBar;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.RecyclerView;

import com.serenegiant.usb.CameraDialog;
import com.serenegiant.usb.IButtonCallback;
import com.serenegiant.usb.Size;
import com.serenegiant.usb.USBMonitor;
import com.serenegiant.usb.common.AbstractUVCCameraHandler;
import com.serenegiant.usb.encoder.RecordParams;
import com.serenegiant.usb.widget.CameraViewInterface;
import com.serenegiant.usb.widget.UVCCameraTextureView;
import com.zhihu.matisse.Matisse;
import com.zhihu.matisse.MimeType;
import com.zhihu.matisse.engine.impl.GlideEngine;
import com.zhihu.matisse.filter.Filter;
import com.zpfeng.oralcamera.R;
import com.zpfeng.oralcamera.UVCCameraHelper;
import com.zpfeng.oralcamera.application.MyApplication;
import com.zpfeng.oralcamera.utils.FileUtils;
import com.zpfeng.oralcamera.utils.Share2;
import com.zpfeng.oralcamera.utils.ShareContentType;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import static java.lang.Double.parseDouble;


public class OralCameraActivity extends AppCompatActivity implements CameraDialog.CameraDialogParent, CameraViewInterface.Callback {
    private static final String TAG = "Debug";
    private static final int REQUEST_CODE_CHOOSE = 23;
    @BindView(R.id.camera_view)
    public UVCCameraTextureView mTextureView;
    @BindView(R.id.toolbar)
    public Toolbar mToolbar;
    @BindView(R.id.seekbar_brightness)
    public SeekBar mSeekBrightness;
    @BindView(R.id.seekbar_contrast)
    public SeekBar mSeekContrast;
    @BindView(R.id.capture_pic_button)
    public Button mCapturePicBtn;
    @BindView(R.id.capture_pic_three_button)
    public Button mCapturePicThreeBtn;
    @BindView(R.id.seekbar_rotate)
    public SeekBar mSeekRotate;
    @BindView(R.id.switch_rec_voice)
    public Switch mSwitchVoice;
    @BindView(R.id.framelayout_view)
    public FrameLayout mFrameLayout;
    @BindView(R.id.etext_patient_name)
    public EditText mETextPatientName;
    @BindView(R.id.etext_patient_temp)
    public EditText mETextPatientTemp;

//    BluetoothAdapter mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();

    public String mPicBasePath = Environment.getExternalStorageDirectory()
            + File.separator + Environment.DIRECTORY_DCIM
            + File.separator;
    public String mPicPath = null;
    public String mWaterMark = null;

    private int mTakePicInterval = 500;
    private AbstractUVCCameraHandler.OnCaptureListener mCaptureListener;
    private UVCCameraHelper mCameraHelper;
    private CameraViewInterface mUVCCameraView;
    private AlertDialog mDialog;
    private AlertDialog mIntervalDialog;

    private boolean isRequest;
    private boolean isPreview;

    private UVCCameraHelper.OnMyDevConnectListener listener = new UVCCameraHelper.OnMyDevConnectListener() {

        @Override
        public void onAttachDev(UsbDevice device) {
            // request open permission
            if (!isRequest) {
                isRequest = true;
                if (mCameraHelper != null) {
                    mCameraHelper.requestPermission(0);
                }
            }
        }

        @Override
        public void onDettachDev(UsbDevice device) {
            // close camera
            if (isRequest) {
                isRequest = false;
                mCameraHelper.closeCamera();
                showLongMsg(device.getDeviceName() + getResources().getString(R.string.is_disconnect));
            }
        }

        @Override
        public void onConnectDev(UsbDevice device, boolean isConnected) {
            if (!isConnected) {
                showLongMsg(getResources().getString(R.string.conn_fail_check_param));
                isPreview = false;
            } else {
                isPreview = true;
                showLongMsg(getResources().getString(R.string.connecting));
                // initialize seekbar
                // need to wait UVCCamera initialize over
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            Thread.sleep(2500);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                        Looper.prepare();
                        if(mCameraHelper != null && mCameraHelper.isCameraOpened()) {
                            mSeekBrightness.setProgress(mCameraHelper.getModelValue(UVCCameraHelper.MODE_BRIGHTNESS));
                            mSeekContrast.setProgress(mCameraHelper.getModelValue(UVCCameraHelper.MODE_CONTRAST));
                            mCameraHelper.setButtonCallback(new IButtonCallback() {
                                @Override
                                public void onButton(final int button, final int state) {
                                    runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
//                                            final Toast toast = Toast.makeText(OralCameraActivity.this, "onButton(button=" + button + "; " +
//                                                    "state=" + state + ")", Toast.LENGTH_SHORT);
//                                            toast.show();
                                            mCapturePicBtn.setEnabled(false);
                                            mCapturePicThreeBtn.setEnabled(false);
                                            mCaptureListener = new AbstractUVCCameraHandler.OnCaptureListener() {
                                                @Override
                                                public void onCaptureResult(String path) {
                                                    Log.i(TAG,getResources().getString(R.string.save_path) + path);
                                                    showLongMsg(getResources().getString(R.string.already_save_path) + path);
                                                }
                                            };
                                            Handler handler = new Handler();
                                            handler.postDelayed(new Runnable() {
                                                @Override
                                                public void run() {
                                                    mPicPath = getPicPath();
                                                    mWaterMark = getWaterMark();
                                                    mCameraHelper.capturePicture(OralCameraActivity.this, mPicPath, mWaterMark, mCaptureListener);
                                                }
                                            }, 10);
                                            handler.postDelayed(new Runnable() {
                                                @Override
                                                public void run() {
                                                    mPicPath = getPicPath();
                                                    mWaterMark = getWaterMark();
                                                    mCameraHelper.capturePicture(OralCameraActivity.this, mPicPath, mWaterMark, mCaptureListener);
                                                }
                                            }, mTakePicInterval);
                                            handler.postDelayed(new Runnable() {
                                                @Override
                                                public void run() {
                                                    mPicPath = getPicPath();
                                                    mWaterMark = getWaterMark();
                                                    mCameraHelper.capturePicture(OralCameraActivity.this, mPicPath, mWaterMark, mCaptureListener);
                                                    // just to prevent multiple click
                                                    mCapturePicBtn.setEnabled(true);
                                                    mCapturePicThreeBtn.setEnabled(true);
                                                }
                                            }, mTakePicInterval * 2);
                                        }
                                    });
                                }
                            });
                        }
                        Looper.loop();
                    }
                }).start();
            }
        }

        @Override
        public void onDisConnectDev(UsbDevice device) {
            showLongMsg(getResources().getString(R.string.disconnecting));
        }
    };

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_usbcamera);
        ButterKnife.bind(this);
        initView();

//        BleManager.getInstance().init(getApplication());
//        BleManager.getInstance()
//                .enableLog(true)
//                .setReConnectCount(1, 5000)
//                .setConnectOverTime(20000)
//                .setOperateTimeout(5000);

        // step.1 initialize UVCCameraHelper
        mUVCCameraView = (CameraViewInterface) mTextureView;
        mUVCCameraView.setCallback(this);
        mCameraHelper = UVCCameraHelper.getInstance();
        mCameraHelper.setDefaultFrameFormat(UVCCameraHelper.FRAME_FORMAT_MJPEG);
        mCameraHelper.initUSBMonitor(this, mUVCCameraView, listener);


        mCameraHelper.setOnPreviewFrameListener(new AbstractUVCCameraHandler.OnPreViewResultListener() {
            @Override
            public void onPreviewResult(byte[] nv21Yuv) {
                Log.d(TAG, "onPreviewResult: "+nv21Yuv.length);
            }
        });
    }

    private String getPicPath() {
        String picPath = mPicBasePath;
        String patient_name = mETextPatientName.getText().toString();
        if (patient_name.length() > 0) {
            picPath += patient_name + "_";
        }
        picPath += getResources().getString(R.string.mouth_capture);
        long time = System.currentTimeMillis();
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd-HH:mm:ss:SSS");
        String curr_t = format.format(new Date(time));
        picPath += curr_t + UVCCameraHelper.SUFFIX_JPEG;
        return picPath;
    }

    private String getWaterMark() {
        String patient_name = mETextPatientName.getText().toString();
        String patient_temp = mETextPatientTemp.getText().toString();
        String water_mark = "";
        if (patient_name.length() > 0) {
            water_mark += patient_name + " ";
        }
        if (patient_temp.length() > 0) {
            water_mark += patient_temp + getResources().getString(R.string.temp_cel) + " ";
            //showLongMsg(water_mark);
        }
        water_mark += getResources().getString(R.string.mouth_capture) + " ";
        long time = System.currentTimeMillis();
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String curr_t = format.format(new Date(time));
        water_mark += curr_t;
        return water_mark;
    }

    private void initView() {
        setSupportActionBar(mToolbar);

        mTextureView.setOnClickListener(new TextureView.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mCameraHelper == null || !mCameraHelper.isCameraOpened()) {
                    showLongMsg(getResources().getString(R.string.open_camera_failed));
                } else {
                    showShortMsg(getResources().getString(R.string.focusing));
                    mCameraHelper.startCameraFoucs();
                }
            }
        });

        mSeekBrightness.setMax(100);
        mSeekBrightness.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                if(mCameraHelper != null && mCameraHelper.isCameraOpened()) {
                    mCameraHelper.setModelValue(UVCCameraHelper.MODE_BRIGHTNESS,progress);
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
        mSeekContrast.setMax(100);
        mSeekContrast.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                if(mCameraHelper != null && mCameraHelper.isCameraOpened()) {
                    mCameraHelper.setModelValue(UVCCameraHelper.MODE_CONTRAST,progress);
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
        mCapturePicBtn.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mCameraHelper == null || !mCameraHelper.isCameraOpened()) {
                    showLongMsg(getResources().getString(R.string.open_camera_failed));
                    return;
                }
                mCapturePicBtn.setEnabled(false);
                mCapturePicThreeBtn.setEnabled(false);
                mPicPath = getPicPath();
                mWaterMark = getWaterMark();
                mCameraHelper.capturePicture(OralCameraActivity.this, mPicPath, mWaterMark,
                        new AbstractUVCCameraHandler.OnCaptureListener() {
                    @Override
                    public void onCaptureResult(String path) {
                        Log.i(TAG,getResources().getString(R.string.save_path) + path);
                        showLongMsg(getResources().getString(R.string.already_save_path) + path);
                    }
                });
                // just to prevent multiple click
                mCapturePicBtn.setEnabled(true);
                mCapturePicThreeBtn.setEnabled(true);
            }
        });

        mCapturePicThreeBtn.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mCameraHelper == null || !mCameraHelper.isCameraOpened()) {
                    showLongMsg(getResources().getString(R.string.open_camera_failed));
                    return;
                }
                mCapturePicBtn.setEnabled(false);
                mCapturePicThreeBtn.setEnabled(false);
                mCaptureListener = new AbstractUVCCameraHandler.OnCaptureListener() {
                    @Override
                    public void onCaptureResult(String path) {
                        Log.i(TAG,getResources().getString(R.string.save_path) + path);
                        showLongMsg(getResources().getString(R.string.already_save_path) + path);
                    }
                };
                Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        mPicPath = getPicPath();
                        mWaterMark = getWaterMark();
                        mCameraHelper.capturePicture(OralCameraActivity.this, mPicPath, mWaterMark, mCaptureListener);
                    }
                }, 10);
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        mPicPath = getPicPath();
                        mWaterMark = getWaterMark();
                        mCameraHelper.capturePicture(OralCameraActivity.this, mPicPath, mWaterMark, mCaptureListener);
                    }
                }, mTakePicInterval);
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        mPicPath = getPicPath();
                        mWaterMark = getWaterMark();
                        mCameraHelper.capturePicture(OralCameraActivity.this, mPicPath, mWaterMark, mCaptureListener);
                        // just to prevent multiple click
                        mCapturePicBtn.setEnabled(true);
                        mCapturePicThreeBtn.setEnabled(true);
                    }
                }, mTakePicInterval * 2);
            }
        });

        mSeekRotate.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                if (progress == 1) {
                    mTextureView.setRotation(0);
                } else if (progress == 2) {
                    mTextureView.setRotation(90);
                } else if (progress == 3) {
                    mTextureView.setRotation(270);
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        mETextPatientTemp.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    String ss = ((EditText) v).getText().toString();
                    if (ss.length() == 0) {
                        return;
                    }
                    double val = parseDouble(ss);
                    if (val < 35.0) {
                        mETextPatientTemp.setText(getResources().getString(R.string.temp_lower_bound));
                    } else if (val > 42.0) {
                        mETextPatientTemp.setText(getResources().getString(R.string.temp_upper_bound));
                    }
                }
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        // step.2 register USB event broadcast
        if (mCameraHelper != null) {
            mCameraHelper.registerUSB();
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        // step.3 unregister USB event broadcast
        if (mCameraHelper != null) {
            mCameraHelper.unregisterUSB();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_toobar, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_takepic:
                if (mCameraHelper == null || !mCameraHelper.isCameraOpened()) {
                    showLongMsg(getResources().getString(R.string.open_camera_failed));
                    return super.onOptionsItemSelected(item);
                }
//                String picPath = UVCCameraHelper.ROOT_PATH + MyApplication.DIRECTORY_NAME +"/images/"+ System.currentTimeMillis()
//                        + UVCCameraHelper.SUFFIX_JPEG;
                mPicPath = getPicPath();
                mWaterMark = getWaterMark();
                //showLongMsg("save path: " + picPath);
                mCameraHelper.capturePicture(this, mPicPath, mWaterMark,
                        new AbstractUVCCameraHandler.OnCaptureListener() {
                            @Override
                            public void onCaptureResult(String path) {
                                Log.i(TAG, getResources().getString(R.string.save_path) + path);
                                showLongMsg(getResources().getString(R.string.already_save_path) + path);
                            }
                        });

                break;
            case R.id.menu_recording:
                if (mCameraHelper == null || !mCameraHelper.isCameraOpened()) {
                    showLongMsg(getResources().getString(R.string.open_camera_failed));
                    return super.onOptionsItemSelected(item);
                }
                if (!mCameraHelper.isPushing()) {
                    String videoPath = UVCCameraHelper.ROOT_PATH + MyApplication.DIRECTORY_NAME + "/videos/" + System.currentTimeMillis();
                    FileUtils.createfile(FileUtils.ROOT_PATH + "test666.h264");
                    // if you want to record,please create RecordParams like this
                    RecordParams params = new RecordParams();
                    params.setRecordPath(videoPath);
                    params.setRecordDuration(0);                        // 设置为0，不分割保存
                    params.setVoiceClose(mSwitchVoice.isChecked());    // is close voice
                    mCameraHelper.startPusher(params, new AbstractUVCCameraHandler.OnEncodeResultListener() {
                        @Override
                        public void onEncodeResult(byte[] data, int offset, int length, long timestamp, int type) {
                            // type = 1,h264 video stream
                            if (type == 1) {
                                FileUtils.putFileStream(data, offset, length);
                            }
                            // type = 0,aac audio stream
//                            if(type == 0) {
//
//                            }
                        }

                        @Override
                        public void onRecordResult(String videoPath) {
                            Log.i(TAG, getResources().getString(R.string.video_path) + videoPath);
                        }
                    });
                    // if you only want to push stream,please call like this
                    // mCameraHelper.startPusher(listener);
                    showLongMsg(getResources().getString(R.string.start_record));
                    mSwitchVoice.setEnabled(false);
                } else {
                    FileUtils.releaseFile();
                    mCameraHelper.stopPusher();
                    showLongMsg(getResources().getString(R.string.stop_record));
                    mSwitchVoice.setEnabled(true);
                }
                break;
            case R.id.menu_resolution:
                if (mCameraHelper == null || !mCameraHelper.isCameraOpened()) {
                    showLongMsg(getResources().getString(R.string.open_camera_failed));
                    return super.onOptionsItemSelected(item);
                }
                showResolutionListDialog();
                break;
            case R.id.menu_focus:
                if (mCameraHelper == null || !mCameraHelper.isCameraOpened()) {
                    showLongMsg(getResources().getString(R.string.open_camera_failed));
                    return super.onOptionsItemSelected(item);
                }
                mCameraHelper.startCameraFoucs();
                break;
            case R.id.share_pictures:
                Matisse.from(OralCameraActivity.this)
                        .choose(MimeType.ofJPEG(), false)
                        .theme(R.style.Matisse_Dracula)
                        .countable(true)
//                        .capture(true)
//                        .captureStrategy(
//                                new CaptureStrategy(true, "com.zhihu.matisse.sample.fileprovider", "test"))
                        .maxSelectable(32)
                        .addFilter(new GifSizeFilter(320, 320, 5 * Filter.K * Filter.K))
                        .gridExpectedSize(
                                getResources().getDimensionPixelSize(R.dimen.grid_expected_size))
                        .restrictOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT)
                        .thumbnailScale(0.85f)
                        .imageEngine(new GlideEngine())
//                        .setOnSelectedListener((uriList, pathList) -> {
//                            Log.e("onSelected", "onSelected: pathList=" + pathList);
//                        })
                        .showSingleMediaType(true)
                        .originalEnable(true)
                        .maxOriginalSize(10)
                        .autoHideToolbarOnSingleTap(true)
//                        .setOnCheckedListener(isChecked -> {
//                            Log.e("isChecked", "onCheck: isChecked=" + isChecked);
//                        })
                        .forResult(REQUEST_CODE_CHOOSE);
                mAdapter.setData(null, null);
                break;
            case R.id.takepic_interval:
                showTakePicIntervalDialog();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private UriAdapter mAdapter = new UriAdapter();

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE_CHOOSE && resultCode == RESULT_OK) {
            List<Uri> uris = Matisse.obtainResult(data);
            if (uris != null && uris.size() > 0) {
                new Share2.Builder(this)
                        .setContentType(ShareContentType.IMAGE)
                        .setShareFileUris(uris)
                        .setTitle(getResources().getString(R.string.share_picture))
                        .build()
                        .shareBySystem();
            }
            mAdapter.setData(uris, Matisse.obtainPathResult(data));
            //Log.e("OnActivityResult ", String.valueOf(Matisse.obtainOriginalState(data)));
        }
    }

    static class UriAdapter extends RecyclerView.Adapter<UriAdapter.UriViewHolder> {

        private List<Uri> mUris;
        private List<String> mPaths;

        void setData(List<Uri> uris, List<String> paths) {
            mUris = uris;
            mPaths = paths;
            notifyDataSetChanged();
        }

        @Override
        public UriViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            return new UriViewHolder(
                    LayoutInflater.from(parent.getContext()).inflate(R.layout.uri_item, parent, false));
        }

        @Override
        public void onBindViewHolder(UriViewHolder holder, int position) {
            holder.mUri.setText(mUris.get(position).toString());
            holder.mPath.setText(mPaths.get(position));

            holder.mUri.setAlpha(position % 2 == 0 ? 1.0f : 0.54f);
            holder.mPath.setAlpha(position % 2 == 0 ? 1.0f : 0.54f);
        }

        @Override
        public int getItemCount() {
            return mUris == null ? 0 : mUris.size();
        }

        static class UriViewHolder extends RecyclerView.ViewHolder {

            private TextView mUri;
            private TextView mPath;

            UriViewHolder(View contentView) {
                super(contentView);
                mUri = (TextView) contentView.findViewById(R.id.uri);
                mPath = (TextView) contentView.findViewById(R.id.path);
            }
        }
    }

    private void showTakePicIntervalDialog() {
        String int_str = String.valueOf(mTakePicInterval);
        View rootView = LayoutInflater.from(OralCameraActivity.this).inflate(R.layout.takepic_interval_dialog, null);
        EditText editText = rootView.findViewById(R.id.takepic_interval_edittext);
        editText.setInputType(InputType.TYPE_CLASS_NUMBER);
        editText.setText(int_str.toCharArray(), 0, int_str.length());
        AlertDialog.Builder builder =
                new AlertDialog.Builder(OralCameraActivity.this);
        Button confirm = rootView.findViewById(R.id.takepic_interval_confirm);
        confirm.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View v) {
                mTakePicInterval = Integer.parseInt(editText.getText().toString());
                Toast.makeText(OralCameraActivity.this,
                        getResources().getString(R.string.takepic_interval_set_note) +
                                editText.getText().toString() + "ms",
                        Toast.LENGTH_SHORT).show();
                mIntervalDialog.dismiss();
            }
        });
        mIntervalDialog = builder.setView(rootView).create();
        mIntervalDialog.show();
    }

    private void showResolutionListDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(OralCameraActivity.this);
        View rootView = LayoutInflater.from(OralCameraActivity.this).inflate(R.layout.layout_dialog_list, null);
        ListView listView = rootView.findViewById(R.id.listview_dialog);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(OralCameraActivity.this, R.layout.resolution_list_item, getResolutionList());
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                if (mCameraHelper == null || !mCameraHelper.isCameraOpened())
                    return;
                final String resolution = (String) adapterView.getItemAtPosition(position);
                String[] tmp = resolution.split("x");
                if (tmp.length >= 2) {
                    int width = Integer.valueOf(tmp[0]);
                    int height = Integer.valueOf(tmp[1]);
                    mCameraHelper.updateResolution(width, height);
                    mCameraHelper.setButtonCallback(new IButtonCallback() {
                        @Override
                        public void onButton(final int button, final int state) {
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
//                                            final Toast toast = Toast.makeText(OralCameraActivity.this, "onButton(button=" + button + "; " +
//                                                    "state=" + state + ")", Toast.LENGTH_SHORT);
//                                            toast.show();
                                    mCapturePicBtn.setEnabled(false);
                                    mCapturePicThreeBtn.setEnabled(false);
                                    mCaptureListener = new AbstractUVCCameraHandler.OnCaptureListener() {
                                        @Override
                                        public void onCaptureResult(String path) {
                                            Log.i(TAG,getResources().getString(R.string.save_path) + path);
                                            showLongMsg(getResources().getString(R.string.already_save_path) + path);
                                        }
                                    };
                                    Handler handler = new Handler();
                                    handler.postDelayed(new Runnable() {
                                        @Override
                                        public void run() {
                                            mPicPath = getPicPath();
                                            mWaterMark = getWaterMark();
                                            mCameraHelper.capturePicture(OralCameraActivity.this, mPicPath, mWaterMark, mCaptureListener);
                                        }
                                    }, 10);
                                    handler.postDelayed(new Runnable() {
                                        @Override
                                        public void run() {
                                            mPicPath = getPicPath();
                                            mWaterMark = getWaterMark();
                                            mCameraHelper.capturePicture(OralCameraActivity.this, mPicPath, mWaterMark, mCaptureListener);
                                        }
                                    }, mTakePicInterval);
                                    handler.postDelayed(new Runnable() {
                                        @Override
                                        public void run() {
                                            mPicPath = getPicPath();
                                            mWaterMark = getWaterMark();
                                            mCameraHelper.capturePicture(OralCameraActivity.this, mPicPath, mWaterMark, mCaptureListener);
                                            // just to prevent multiple click
                                            mCapturePicBtn.setEnabled(true);
                                            mCapturePicThreeBtn.setEnabled(true);
                                        }
                                    }, mTakePicInterval * 2);
                                }
                            });
                        }
                    });
                }
                mDialog.dismiss();
            }
        });
        builder.setView(rootView);
        mDialog = builder.create();
        mDialog.show();
    }

    // example: {640x480,320x240,etc}
    private List<String> getResolutionList() {
        List<Size> list = mCameraHelper.getSupportedPreviewSizes();
        List<String> resolutions = null;
        if (list != null && list.size() != 0) {
            resolutions = new ArrayList<>();
            for (Size size : list) {
                if (size != null) {
                    resolutions.add(size.width + "x" + size.height);
                }
            }
        }
        return resolutions;
    }

    private Size getMaximumResolution() {
        Size res = null;
        List<Size> list = mCameraHelper.getSupportedPreviewSizes();
        for (Size size : list) {
            if (size != null) {
                if (res == null) {
                    res = size;
                } else if (size.width * size.height > res.width * res.height) {
                    res = size;
                }
            }
        }
        return res;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        FileUtils.releaseFile();
        // step.4 release uvc camera resources
        if (mCameraHelper != null) {
            mCameraHelper.release();
        }
//        BleManager.getInstance().disconnectAllDevice();
//        BleManager.getInstance().destroy();
    }

    private void showLongMsg(String msg) {
        //Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
        Toast.makeText(this, msg, Toast.LENGTH_LONG).show();
    }

    private void showShortMsg(String msg) {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
    }

    @Override
    public USBMonitor getUSBMonitor() {
        return mCameraHelper.getUSBMonitor();
    }

    @Override
    public void onDialogResult(boolean canceled) {
        if (canceled) {
            showLongMsg("取消操作");
        }
    }

    public boolean isCameraOpened() {
        return mCameraHelper.isCameraOpened();
    }

    @Override
    public void onSurfaceCreated(CameraViewInterface view, Surface surface) {
        if (!isPreview && mCameraHelper.isCameraOpened()) {
            mCameraHelper.startPreview(mUVCCameraView);
            isPreview = true;
        }
    }

    @Override
    public void onSurfaceChanged(CameraViewInterface view, Surface surface, int width, int height) {

    }

    @Override
    public void onSurfaceDestroy(CameraViewInterface view, Surface surface) {
        if (isPreview && mCameraHelper.isCameraOpened()) {
            mCameraHelper.stopPreview();
            isPreview = false;
        }
    }
//
//    public boolean isSupportBluetooth() {
//        return mBluetoothAdapter != null;
//    }
//    public boolean isBluetoothOpen() {
//        if (mBluetoothAdapter != null) {
//            return mBluetoothAdapter.isEnabled();
//        }
//        return false;
//    }
//    public void enableBluetooth() {
//        if (isSupportBluetooth() && !isBluetoothOpen()) {
//            // force to open bluetooth
//            mBluetoothAdapter.enable();
//        }
//    }
//    public List<BluetoothDevice> checkBluetoothDevices() {
//        List<BluetoothDevice> devices = new ArrayList<>();
//        if(mBluetoothAdapter!=null){
//            Set<BluetoothDevice> pairedDevices = mBluetoothAdapter.getBondedDevices();
//            if (pairedDevices != null && pairedDevices.size() > 0) {
//                devices.addAll(pairedDevices);
//            }
//        }
//        return devices;
//    }
//    public void findBluetoothDevice() {
//        if(mBluetoothAdapter!=null && mBluetoothAdapter.isEnabled() && !mBluetoothAdapter.isDiscovering()){
//            if (mBluetoothAdapter.startDiscovery()) {
//                showLongMsg(getResources().getString(R.string.bluetooth_start_search));
//            } else {
//                showLongMsg(getResources().getString(R.string.bluetooth_start_search_fail));
//            }
//        }
//    }
}
